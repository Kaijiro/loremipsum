"use strict";

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse){
  if(message != null && message.text.length != 0){
    document.execCommand("paste");
    sendResponse();
  }
});
