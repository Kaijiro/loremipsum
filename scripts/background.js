"use strict";

var bgp = chrome.extension.getBackgroundPage();

chrome.browserAction.onClicked.addListener(function (tab) {
        var loremChoosed = localStorage["loremChoosed"];

        var data = getClipboardData();

        // TODO Something like getLorem(loremChoosed)
        var loremContent = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        copyToClipboard(loremContent);

        chrome.tabs.sendMessage(tab.id, {text : "clipboardReady"}, function(){
          copyToClipboard(data);
        });
});

function getClipboardData(){
  // Create a temporary textarea to get user clipboard content because we don't want the user to loose it
  var textarea = document.createElement("textarea");

  // Append it in the background page
  document.body.appendChild(textarea);

  // Focus the textarea, so we can write inside
  textarea.focus();

  // Paste clipboard content
  document.execCommand("paste");

  // Store clipboard content inside a var
  var data = textarea.value;

  // Remove the textarea, we don't need it anymore
  document.body.removeChild(textarea);

  // Return clipboard content
  return data;
}

function copyToClipboard(data){
  // Create a temporary textarea to get user clipboard content because we don't want the user to loose it
  var textarea = document.createElement("textarea");

  // Append it in the background page
  document.body.appendChild(textarea);

  // Focus the textarea, so we can copy from it
  textarea.focus();

  // Copy data inside the textarea
  textarea.value = data;

  // Select all text and copy it
  textarea.select();

  // Remove the textarea
  document.body.removeChild(textarea);
}
