$(document).on("ready", function(){
  "use strict";

  // TODO : Display last selected Lorem (if no last selected, select classic)

  $("#loremChooserForm").on("submit", function(){
    var serialized = $(this).serializeArray();

    if(serialized.length == 0){
      // TODO : Display error
      alert("Select a Lorem Ipsum");
      return false;
    }

    // TODO : Maybe change to chrome.storage : Faster, async (read the doc)
    localStorage["loremChoosed"] = serialized[0].value;
    // TODO : Display "success !"
    alert("Lorem Ipsum changed to : " + localStorage["loremChoosed"]);
    return false;
  });
});
